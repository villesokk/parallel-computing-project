{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}

module NearestNeighbour where

import Control.Monad (replicateM, forM_)
import Data.Array.Accelerate as A hiding (length)
import qualified Data.Array.Accelerate.CUDA as CUDA
import qualified Data.Array.Accelerate.Interpreter as I
import Data.Bits ((.&.), Bits)
import Prelude as P
import qualified Data.List as L
import Data.List.Split (chunksOf)
import System.Random.MWC
import Text.Printf (printf)


--
-- The sorting functions work on vectors Elt a => Acc (Vector (a,
-- Float)). a is the type of the element (for example, index in the
-- data set) and Float is the distance which is used for ordering.
--

type SortingFunction
  = forall a. Elt a =>
    Int -> Int -> Acc (Vector (a, Float)) -> Acc (Vector (a, Float))

powerOf2 :: (Num a, Bits a) => a -> Bool
powerOf2 x = x .&. (x - 1) == 0

toVec :: Elt a => [a] -> Acc (Vector a)
toVec lst = use $ fromList (Z :. length lst) lst

matrix :: Elt a => [[a]] -> Acc (Array DIM2 a)
matrix lsts = use . fromList (Z :. m :. n) $ concat lsts
  where
    m = length lsts
    n = length $ head lsts

-- 'unit' creates a scalar but this creates a vector with a single
-- element
singleton :: Elt t => Exp t -> Acc (Vector t)
singleton x = fill (constant (Z :. (1 :: Int))) x

-- Full insertion sort
insertionSort :: forall a. Elt a =>
                 Acc (Vector (a, Float)) -> Acc (Vector (a, Float))
insertionSort vec =
  awhile (\res -> unit $ size res /=* n)
  (\res -> insert (vec A.!! size res) res)
  empty
  where
    n     = size vec
    empty = A.take 0 vec

insert :: (Elt a, Elt d, IsScalar d) =>
          Exp (a, d) -> Acc (Vector (a, d)) -> Acc (Vector (a, d))
insert input vec = res
  where
    dist       = A.snd input
    def        = fill (lift $ Z :. size vec + 1) input
    indices    = enumFromN (shape vec) 0
    -- indices in the result vector
    to         = A.zipWith zipper vec indices
    zipper x i = cond (A.snd x <* dist) i (i + 1)
    -- copy to the correct position in the result vector
    res        = scatter to def vec

-- Sorts the first k elements, then inserts latter elements into the
-- k-element vector if they are smaller than the greatest element.
insertionSortK :: SortingFunction
insertionSortK k n input =
  foldl1 (>->) [ maybeInsert i | i <- [k .. n - 1] ] initial
  where
    initial = insertionSort $ A.take (constant k) input

    maybeInsert i vec =
      let elem = input A.!! constant i
          last = vec A.!! (size vec - 1)
      in
       acond (A.snd last >* A.snd elem)
       (insert elem $ A.init vec)
       vec

sortKMatRows :: (Elt a, Elt d, IsScalar d) =>
                (Int -> Int -> Acc (Vector (a, d)) -> Acc (Vector (a, d))) ->
                Int -> Int -> Int -> Acc (Array DIM2 (a, d)) ->
                Acc (Array DIM2 (a, d))
sortKMatRows sortK k m n mat =
  reshape (lift (Z :. m :. k)) $
  foldl1 (A.++) [ sortRow i | i <- [0 .. m - 1] ]
  where
    sortRow i = sortK k n $ slice mat (lift (Z :. i :. All))

split :: [a] -> Bool -> ([a], [a]) -> ([a], [a])
split [] _ (even, odd) = (P.reverse even, P.reverse odd)
split (x:xs) isEven (even, odd) =
  if isEven then
    split xs False (x:even, odd)
  else
    split xs True (even, x:odd)

type Stage = [(Int, Int)]
type Network = [Stage]

simplify :: Network -> Network
simplify network
  | count == 0 = network'
  | otherwise  = P.fst $ simplify' network' ([], 0)
  where
    (network', count) = simplify' network ([], 0)

simplify' :: Network -> (Network, Int) -> (Network, Int)
simplify' [] accumulator = accumulator
simplify' [stage] (network, count) = (stage : network, count)
simplify' (stage1 : stage2 : rest) (result, count)
  | P.null (s1flat `L.intersect` s2flat) =
      simplify' rest (((stage1 P.++ stage2) : result), count + 1)

  | otherwise =
      simplify' (stage2 : rest) ((stage1 : result), count)

  where
    s1flat = flatten stage1
    s2flat = flatten stage2
    flatten stage = concatMap (\(a, b) -> [a, b]) stage

-- NOTE: the list of stages is reversed
makeNetwork :: Int -> Network
makeNetwork n
  | n == 2 = [[(0, 1)]]
  | otherwise = result
  where
    nHalf = n `div` 2
    firstHalf = makeNetwork nHalf
    secondHalf = P.map incrementGates firstHalf
    incrementGates stage = P.map (\(a, b) -> (a + nHalf, b + nHalf)) stage
    concatenated = P.zipWith (P.++) firstHalf secondHalf
    leftIdx = [0 .. nHalf - 1]
    rightIdx = [nHalf .. n - 1]
    result = mergeNetworks concatenated n leftIdx rightIdx

mergeNetworks :: Network -> Int -> [Int] -> [Int] -> Network
mergeNetworks network _ [] [] = network
mergeNetworks network _ [l] [r] = [(l, r)] : network
mergeNetworks network n left right =
  result
  where
    (leftEven, leftOdd) = split left True ([], [])
    (rightEven, rightOdd) = split right True ([], [])
    nHalf = n `div` 2
    neven = mergeNetworks network nHalf leftEven rightEven
    nodd = mergeNetworks neven nHalf leftOdd rightOdd
    evens = P.tail leftEven P.++ rightEven
    odds = leftOdd P.++ P.init rightOdd
    lastStage = P.zip odds evens
    result = lastStage : nodd

generateOddEvenNetwork :: Int -> Network
generateOddEvenNetwork n
  | P.not (powerOf2 n && n > 1) =
      error "generateOddEvenNetwork: input size must be greater than one and a power of two"

  | otherwise = simplify . P.reverse $ makeNetwork n

printNetwork :: Network -> IO ()
printNetwork stages = do
  putStrLn . printf "Number of stages: %d" $ length stages
  forM_ stages $ \stage -> do
    putStrLn . L.intercalate "\n" $ P.map showPair stage
    putStrLn ""

  where
    showPair (a, b) = concat [show a, " ", show b]

oddEvenSortK :: Network -> SortingFunction
oddEvenSortK network k _ input =
  A.take (constant k) sorted
  where
    sorted = P.foldl1 (>->) [ sortStage stage | stage <- network ] input

sortStage :: (Elt a, Elt d, IsScalar d) =>
             Stage -> Acc (Vector (a, d)) -> Acc (Vector (a, d))
sortStage stage input =
  result
  where
    minIdxs = toVec $ P.map P.fst stage
    maxIdxs = toVec $ P.map P.snd stage
    mins = A.map (\i -> input A.!! i) minIdxs
    maxs = A.map (\i -> input A.!! i) maxIdxs
    compare a b = cond (A.snd a <* A.snd b) (lift (a, b)) (lift (b, a))
    compared = A.zipWith compare mins maxs
    minsRes = A.map A.fst compared
    maxsRes = A.map A.snd compared
    result = scatter maxIdxs (scatter minIdxs input minsRes) maxsRes

-- nQuer        - number of query points
-- nPoints      - number of data points
-- queries      - each row is a query point
-- points       - each row is a data point
-- return value - row i contains the k nearest neighbours of query i
nearestNeighbours ::
  -- We constrain the sorting function type because type inference
  -- with Rank N types is poor. In production, a better interface
  -- should be designed but for benchmarking, I want to be able to
  -- evaluate the network of the odd-even sorting method before
  -- evaluating this function so I pass in the sorting function.
  (Int -> Int -> Acc (Vector (Int, Float)) -> Acc (Vector (Int, Float))) ->
   Int -> Int -> Int -> Int ->
   Acc (Array DIM2 Float) -> Acc (Array DIM2 Float) ->
   Acc (Array DIM3 Float)
nearestNeighbours sortK k dim nQuer nPoints queries points =
  result
  where
    -- Duplicate the references vector to form nQuer columns
    pointsMat = A.replicate (lift (Z :. All :. constant nQuer :. All)) points
    -- Duplicate the queries vector to form nRef rows
    querMat = A.replicate (lift (Z :. constant nPoints :. All :. All)) queries
    -- Calculate the squared differences of the components of all points
    sqrDiffs = A.zipWith (\x y -> square (x - y)) pointsMat querMat
    square x = x * x
    -- Sum the differences of each point
    distMat = A.fold (+) 0 sqrDiffs
    -- Create pairs of indices and distances so that we would know
    -- which points are nearest after top-k sorting
    indices = A.replicate (lift (Z :. All :. constant nQuer)) $
              A.enumFromN (lift (Z :. constant nPoints)) 0
    diffWithIdx = A.zip indices distMat
    -- For each query, find the k best points using sortKMatRows
    sorted = A.map A.fst . sortKMatRows sortK k nQuer nPoints $ transpose diffWithIdx

    -- Copy the best ones to the output
    resShape = lift (Z :. constant nQuer :. constant k :. constant dim)
    result = A.generate resShape $ \ix ->
      -- The types are so generic that type inference does not work so
      -- we have to use these type declarations
      let (Z :. (q :: Exp Int) :. (k :: Exp Int) :. (p :: Exp Int)) = unlift ix
          pointsIdx = sorted A.! (lift (Z :. q :. k))
      in
       points A.! (lift (Z :. pointsIdx :. p))

type Point = [Float]

-- Simple slow reference implementation in Haskell for testing
nnsReference :: Int -> [Point] -> [Point] -> [[Point]]
nnsReference k queries references =
  P.map nns queries
  where
    nns query = P.take k .
                P.map P.fst .
                L.sortBy (\(_, d1) (_, d2) -> compare d1 d2) .
                P.zip references $
                P.map (distance query) references

    distance query point = P.sum $ P.zipWith (\x y -> square (x - y)) query point

    square x = x * x

zeroes :: [Int]
zeroes = repeat 0

testInsert :: IO ()
testInsert =
  test $ insert (lift (0 :: Int, 3 :: Int)) vec
  where
    -- The 'data' is all 0 and is sorted by the distances
    vec = toVec $ P.zip zeroes [1, 2, 4 :: Int]

testInsertionSort :: IO ()
testInsertionSort =
  test . insertionSort . toVec $ P.zip zeroes [2, 3, 1, 4]

testInsertionSortK :: IO ()
testInsertionSortK =
  test . insertionSortK 2 (length lst) $ toVec lst
  where
    lst = P.zip zeroes [2, 3, 1, 4]

testSortKMatRows :: IO ()
testSortKMatRows =
  test $ sortKMatRows insertionSortK 2 2 4 mat
  where
    mat = matrix .
          P.map (P.zip zeroes) $
          [ [30, 10, 20, 40]
          , [2, 4, 3, 1]
          ]

randList :: Int -> IO [Float]
randList length =
  withSystemRandom . asGenIO $ \gen ->
    replicateM length . fmap (*100) $ uniform gen

testNNS :: IO ()
testNNS = do
  queries    <- replicateM 2 $ randList dim
  references <- replicateM 8 $ randList dim

  putStrLn "Queries"
  printPoints queries
  nl

  putStrLn "References"
  printPoints references
  nl

  putStrLn "Correct results:"
  printRes $ nnsReference k queries references
  nl

  let res = I.run (nearestNeighbours (oddEvenSortK network) k dim
                   (length queries) (length references)
                   (matrix queries) (matrix references))
      network = generateOddEvenNetwork $ length references
      resMat = chunksOf (length queries) . chunksOf dim $ toList res

  putStrLn "Accelerate results:"
  printRes resMat

  where
    k              = 2
    dim            = 3
    nl             = putChar '\n'
    printPoints xs = putStrLn . L.intercalate ", " $ P.map showPoint xs
    showPoint xs   = concat ["(", L.intercalate ", " (P.map showFloat xs), ")"]

    printRes xs =
      putStrLn . L.intercalate "\n" $
      P.map (L.intercalate ", " . P.map showPoint) xs

showFloat :: Float -> String
showFloat = printf "%.2f"

testOddEvenSort :: IO ()
testOddEvenSort = do
  list <- randList n
  let withZero = toVec $ P.zip zeroes list
      toLinear = P.map P.snd . toList
      res      = toLinear . I.run $ oddEvenSortK network k n withZero
      correct  = L.sort list

  putStrLn "Correct:"
  printRes correct
  putStrLn "Result:"
  printRes res
  putStrLn $ "Equal? " P.++ show (P.all (P.uncurry (==)) $ P.zip res correct)
  where
    k           = n
    network     = generateOddEvenNetwork n
    n           = 8
    printRes xs = putStrLn . L.intercalate ", " $ P.map showFloat xs

test :: (Show a, Arrays a) => Acc a -> IO ()
test computation =
  print $ I.run computation
