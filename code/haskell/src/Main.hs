module Main where

import Control.Exception (evaluate)
import Control.Monad (when, replicateM, zipWithM, void, forM)
import qualified Data.Array.Accelerate.CUDA as CUDA
import qualified Data.Array.Accelerate.Interpreter as I
import System.Clock
import System.Environment (getArgs)
import System.IO.Error (catchIOError)
import System.Random.MWC
import Text.Read (readMaybe)
import Text.Printf (printf)

import NearestNeighbour


parseInt :: String -> String -> IO Int
parseInt name str = case readMaybe str of
  Just x -> return x
  Nothing -> ioError . userError $ "Couldn't parse number of " ++ name

genPoints :: Int -> Int -> IO [[Float]]
genPoints len dims =
  withSystemRandom . asGenST $
  \gen -> replicateM len . replicateM dims $ uniform gen

timeIt :: IO a -> IO Double
timeIt action = do
  start <- getTime Realtime
  void action
  end <- getTime Realtime

  let diff
        | nsec end - nsec start < 0 =
            TimeSpec { sec  = sec end - sec start - 1
                     , nsec = 1000000000 + nsec end - nsec start
                     }
        | otherwise =
            TimeSpec { sec = sec end - sec start
                     , nsec = nsec end - nsec start
                     }

  return $ fromIntegral (sec diff) + fromIntegral (nsec diff) / 1000000000

-- Number of times to run the benchmark
iterations :: Int
iterations = 10

average :: [Double] -> Double
average xs =
  sum xs / fromIntegral (length xs)

benchmark :: IO ()
benchmark = do
  args <- getArgs

  when (length args /= 5) $
    ioError $ userError "Five arguments are required: sorting function name ('insertion' or 'oddeven'), number of data points, number of query points, number of dimensions, number of nearest neighbours to find (k)"

  let [funStr, pointsStr, queriesStr, dimsStr, kStr] = args

  [pointsNo, queriesNo, dims, k] <-
    zipWithM parseInt ["points", "queries", "dims", "nearest neighbours"]
    [pointsStr, queriesStr, dimsStr, kStr]

  fun <- case funStr of
    "insertion" -> return insertionSortK

    "oddeven" -> do
      let network = generateOddEvenNetwork pointsNo
      void $ evaluate network
      return $ oddEvenSortK network

    _ -> ioError $ userError "Couldn't parse sorting function name"

  times <- forM [0 .. iterations] $ \i -> do
    putStr $ printf "Iteration %d, " i

    points <- genPoints pointsNo dims
    queries <- genPoints queriesNo dims

    let res = CUDA.run (nearestNeighbours fun k dims
                        queriesNo pointsNo
                        (matrix queries) (matrix points))

    time <- timeIt $ evaluate res
    putStrLn $ printf "time %.4fs" time
    return time

  -- We drop the first time. In the succeeding iterations, the CUDA
  -- code is cached and compilation time is not included.
  putStrLn . printf "Average time: %.4fs" . average $ tail times

  return ()

main :: IO ()
main = catchIOError benchmark $ \e -> putStrLn $ show e
