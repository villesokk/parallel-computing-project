This repository contains the University of Tartu Parallel Computing
2014 course project of Ville Sokk. The aim of the project is to become
familiar with a parallel programming framework which in this case is
the Accelerate embedded domain-specific language for Haskell. To learn
Accelerate, an approximate nearest neighbour search algorithm will be
programmed using it. If possible, the algorithm should also be
utilised to do something useful in image processing.
