\documentclass[pdf]{beamer}

\usepackage{algorithm2e}
\usepackage{listings}
\usepackage{amsfonts}
\mode<presentation>{}
\setbeamertemplate{navigation symbols}{}
\def \Oh {\mathcal{O}}

\title{Parallel nearest neighbour search using Accelerate}
\author{Ville Sokk}
\date{}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}{Nearest neighbour search}
  \begin{itemize}
  \item Nearest neighbour search solves the following problem: given a
    point set $P \subset \mathbb{R}^d$ and a query point $q \in
    \mathbb{R}^d$, find the point in $P$ that is closest to $q$ using
    some distance metric.

  \item Multiple queries should be executed in parallel.

  \item Sometimes we need to find the $k$ nearest neighbours.

  \end{itemize}
\end{frame}

\begin{frame}{Accelerate}
  \begin{itemize}
  \item Accelerate is an EDSL (embedded domain specific language) for
    the Haskell programming language which is designed for writing
    data parallel programs.

  \item Supports multiple compiler backends. The same code could be
    compiled to CUDA, OpenCL and CPU code with SIMD instructions.

  \item Code is compiled during run time and can depend on run time
    parameters.

  \item High level pure functional language. This helps avoid bugs and
    the code is easier to reason about.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Accelerate example}
  \begin{lstlisting}
  dotp :: Acc (Vector Float) ->
          Acc (Vector Float) ->
          Acc (Scalar Float)
  dotp xs ys =
    fold (+) 0 (zipWith (*) xs ys)
  \end{lstlisting}
\end{frame}

\begin{frame}{kd-trees}
  A very common solution is the kd-tree data structure. It partitions
  the points spatially and has a search complexity of $\Oh(\log n)$.

  \begin{itemize}
    \item Efficient search is possible on GPUs (there are adaptations
      of it for GPUs).

    \item Becomes very slow when the dimensionality is high.

    \item Efficiently constructing a kd-tree on the GPU is not trivial.
  \end{itemize}
\end{frame}

\begin{frame}{Brute force search}
  Garcia, Vincent, Eric Debreuve, and Michel Barlaud. "Fast k nearest neighbor search using GPU." Computer Vision and Pattern Recognition Workshops, 2008.

  \begin{itemize}
  \item Calculate distances between all queries and data points.

  \item Find the $k$ closest points to each query using modified
    insertion sort.
  \end{itemize}
\end{frame}

\begin{frame}{Brute force search}
  Insertion sorting procedure:
  \begin{itemize}
  \item Sort the first $k$ elements with insertion sort.

  \item Insert every other element into the first $k$ if its distance
    is smaller than the worst distance so far.
  \end{itemize}

  This can be applied in parallel to vectors of each query.
\end{frame}

\begin{frame}{Encountered problems}
  Accelerate supports only flat data parallelism because compiling
  nested data parallel programs to GPUs is very difficult.

  \bigskip
  Parallel combinators can only be passed sequential functions
  operating on scalars.

  \bigskip
  Writing a sequential insertion sort and applying it in parallel was
  not possible.
\end{frame}

\begin{frame}[fragile]
  \frametitle{Encountered problems}
  The parallel combinators abstract most types of parallel loops. But
  there are very limited sequential loop constructs where every
  iteration depends on the previous one.

  \begin{lstlisting}
    insertionSortK k n input =
      foldl1 (>->)
      [ maybeInsert i | i <- [k .. n-1] ]
      initial

      where
        initial =
          insertionSort
            (A.take (constant k) input)

        maybeInsert i vec = ...
  \end{lstlisting}

  With $k = d = number\ of\ queries = 3$ and $n = 256$ it took 297s to
  compile and run the program!
\end{frame}

\begin{frame}[fragile]
  \frametitle{Another approach: Batcher's odd-even merge sort}
  If we can't sort separate vectors in parallel, use a parallel sort
  for each vector. Batcher's odd-even merge sort ($\Oh(n \log(n)^2)$):
  \bigskip

  \begin{algorithm}[H]
  \KwIn{sequence $a_0, \ldots, a_{n-1}$ of length $n > 1$ whose two
  halves $a_0, \ldots, a_{n/2-1}$ and $a_{n/2}, \ldots, a_{n-1}$ are
  sorted ($n$ is a power of two).}

  \eIf{$n > 2$}{
    apply odd-even merge recursively to the even subsequence $a_0,
    a_2, \ldots, a_{n-2}$ and to the odd subsequence $a_1, a_3,
    \ldots, a_{n-1}$; \

    compare $a_i$ and $a_{i+1}$ for all $i \in \{1, 3, 5, \ldots,
    n-3\}$
  }{
    compare $a_0$ and $a_1$
  }
  \end{algorithm}
\end{frame}

\begin{frame}{Sorting networks}
  Using Batcher's odd-even merge sort we can create a graph of
  comparison gates. Independent gates can be evaluated in parallel.

  \begin{figure}[H]
    \begin{center}
      \includegraphics[width=0.3\textwidth]{sortingnetwork.eps}
    \end{center}
  \end{figure}

  Sorting networks are implemented in hardware and on GPUs.
\end{frame}

\begin{frame}[fragile]
  \frametitle{Performance}
  How fast are the programs? I don't know.

  \begin{figure}[H]
    \begin{center}
      \includegraphics[width=0.6\textwidth]{plot.eps}
    \end{center}
  \end{figure}
\end{frame}

\begin{frame}{Conclusion}
  Accelerate is still a research project and I would not use it in
  practice. Things to improve:

  \begin{itemize}
  \item Writing sequential operations on arrays and applying them in
    parallel (might require nested data parallelism support).

  \item Sequential iteration.

  \item Stability.
  \end{itemize}

  What I liked:
  \begin{itemize}
  \item Fewer bugs in code.
  \item Very easy to use interface.
  \end{itemize}
\end{frame}

\end{document}
