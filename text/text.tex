\documentclass{IEEEtran}

\usepackage{algorithm2e}
\usepackage{amsfonts}
\usepackage{listings}
\usepackage{hyperref}
\usepackage{color}
\usepackage{graphicx}


\def \Oh {\mathcal{O}}
\def \code {\texttt}
\newcommand{\todo}[1]{\textcolor{red}{TODO. #1}}


\begin{document}
\title{Parallel nearest neighbour search using Accelerate}
\author{Ville Sokk}

\maketitle

\section{Introduction}

Nearest neighbour search (NN) solves the following problem: given a
point set $P \subset \mathbb{R}^d$ and a query point $q \in
\mathbb{R}^d$, find the point in $P$ that is closest to $q$ using some
distance metric. Nearest neighbour search is used in many different
areas like computer vision, spell checking and DNA sequencing. NN is
often computationally heavy because the size of the point set and the
number of queries tends to be large. When exact nearest neighbour
search is too expensive, approximate nearest neighbour search is used
instead. An extension of the NN problem is the k nearest neighbours
problem where the k closest points need to be found.

The aim of this work is to develop a parallel kNN program using the
Accelerate embedded domain specific language (EDSL) for the Haskell
programming language.


\section{Accelerate}

Accelerate is an EDSL for the Haskell programming language which is
designed for writing data parallel programs \cite{accelerate}. It has
multiple backend compilers targeting different systems. Currently
there are two officially supported backends. One interprets the
program on the CPU and the other is an optimising compiler targeting
the C-like CUDA programming language for Nvidia GPUs
\cite{mcdonell}. There are experimental backend compilers targeting
CPUs (using the LLVM compiler framework) and OpenCL (a C-like language
used for general purpose GPU programming like CUDA).

Accelerate is a pure functional programming language. A function is
pure if its application to some fixed constant arguments can be
replaced with the resulting value. This means that the value of the
function may only depend on its inputs. No input may be read from
somewhere else (no mutable global variables, IO, randomness, etc) and
the only output is the return value. Besides the subjective benefits
touted by proponents of functional programming, this also allows more
agressive optimization by the compiler. A loop that can do IO can not
be safely parallelised by the compiler because the order of IO
operations becomes non-deterministic.

In Accelerate, programs are written using combinator functions like
\code{map}, \code{fold} (called reduce in some languages), \code{zip},
etc. In this context, a combinator function is an abstraction of a
loop. The code that is not common in different applications of the
same loop pattern, is passed to the combinator as a function. For
example, the following is a Haskell/Accelerate code example that
calculates the dot product of two vectors:

\begin{lstlisting}
  dotp :: Acc (Vector Float) ->
          Acc (Vector Float) ->
          Acc (Scalar Float)
  dotp xs ys =
    fold (+) 0 (zipWith (*) xs ys)
\end{lstlisting}

The lines following \code{dotp ::} give the type signature of the
function. It basically states that \code{dotp} is an Accelerate
function that takes two vectors of floating point numbers and produces
a scalar.

First, \code{zipWith} applies the given function (multiplication in
this case) pair-wise to the elements of vectors \code{xs} and
\code{ys}. In Haskell, binary operators can be turned into regular
functions by enclosing them in parentheses. The resulting vector is
reduced by \code{fold} using the given function (addition) and an
initial value of zero. If $x$ is a vector with values $x_1, x_2,
\ldots, x_n$, then \code{fold (+) 0 x} evaluates $0 + x_1 + x_2 +
\ldots + x_n$. Parallelisation is achieved using parallel
implementations of the combinators. Backend compilers targeting
different platforms can use the parallelisation scheme best suited to
the platform.

One of the optimisations that he compiler uses is fusion which can
eliminate intermediate data-structures in a computation by combining
successive traversals \cite{mcdonell}. The following example is a
Haskell code example from \cite{mcdonell}:

\begin{lstlisting}
  sumOfSquares :: Int -> Int
  sumOfSquares n =
    sum (map (\x -> x * x) [1 .. n])
\end{lstlisting}

It creates a list from 1 through \code{n}, computes the square of each
element and then sums the elements of the list. This can be obviously
computed without creating lists/arrays in memory. In a parallel
setting, the goal of fusion is for the compiler to transform this
program so that the semantics remain the same but the program is still
evaluated in parallel, only necessary data structures are created and
no extra traversals are used.

The benefits of Accelerate are:

\begin{itemize}
\item It is a pure functional programming language. Although this is
  not proven empirically, pure functional programs tend to be more
  readable, easier to reason about and contain less mistakes. This
  should mean the programmer is more productive.

\item It has multiple backends and is not tied to a specific
  platform. In some cases, programs must work in heterogenous
  environments. For example, software distributed to end users with
  home computers. The program should work relatively efficiently for
  everyone, including users with Nvidia, AMD, Intel GPUs and users
  without a GPU.

\item The parallel program is compiled during run time of the Haskell
  program. This allows code to be generated that is optimised
  according to some user chosen or data dependent parameters.

\item The de facto standard Glasgow Haskell Compiler also includes an
  interpreter. This makes development more productive since there is
  no separate compilation step.

\item Because the compiler has multiple backends, the developer can
  test their code even when they have no access to the target
  platform.
\end{itemize}

Some of the disadvantages are:

\begin{itemize}
\item The language is very high level while CUDA and OpenCL are
  low-level languages. This results in more readable code and not
  being tied to a platform but it also means that highly optimised
  CUDA/OpenCL code is more efficient.

\item The benefit of being a high level language is limited. Although
  Accelerate also optimises code, it is not perfect. If a programmer
  wishes to optimise their code (which is expected in parallel
  programming), they must still understand how Accelerate and the
  target platform work.

\item The language has some important limitations that make some
  programs difficult to write. These will be discussed in
  section \ref{sec:implementation}.
\end{itemize}


\section{Nearest neighbour search algorithms for GPUs}

The classical algorithm used for nearest neighbour search is the
kd-tree \cite{bentley1975multidimensional} data structure. A kd-tree
is a spatial division of the points which has a search complexity of
$\Oh(\log n)$. There are multiple efficient adaptations of kd-trees
for GPUs, for example the work of Gieske
et.al. \cite{gieseke2014buffer}. But kd-trees have multiple
limitations that make them ill-suited for certain cases of nearest
neighbour search:

\begin{itemize}
\item kd-trees are usually constructed on the CPU and then searched on
  the GPU. This is not suitable when the data is created on the GPU.

\item kd-tree search is fast on low-dimensional data. The algorithm in
  \cite{gieseke2014buffer} is fast with dimensionality up to about
  25. In many domains the points can have hundreds of dimensions.
\end{itemize}

A common approach to k-nearest neighbour search in high dimensions is
the brute force method \cite{garcia2008fast}. The distance between all
query points and all data points is calculated. Then the vectors of
distances are sorted and the best $k$ are picked. Since GPUs have a
lot of cores, the distances can be efficiently calculated in parallel
and the brute force method is a good option when $d$ is high and $n$
is low.

The random ball cover algorithm uses brute force search as a building
block and improves the search complexity to $\Oh(\sqrt{n})$
\cite{cayton2012accelerating}. The basic idea is to first randomly
pick some points from the point set. These are called
representatives. For each point in the point set, the closest
neighbour from the set of representatives is found. These points form
the ownership list of the representative. For each query, brute force
search is used to find the representative and then the closest point
from the list of representatives. This is not exact but the article
explains how to make it so.


\section{Implementation}

\subsection{Implementation using modified insertion sort}

\label{sec:implementation}

At first, the algorithm of \cite{garcia2008fast} was implemented. It
calculates the distances between all pairs of points and
queries. Then, for each query the points are sorted based on distance
and the first $k$ are picked. In this implementation, the first $k$
points are sorted using insertion sort and then each of the other
points are inserted into the first $k-1$ if they are smaller than the
last $k$-th point from the previous iteration. This means that most of
the points are not sorted. The idea is to sort the point vectors of
different queries in parallel.

In Accelerate, there are two types of computations. In the Haskell
type system, the types are \code{Exp a} and \code{Acc a}. Haskell has
parametric types, so \code{a} is some type. The difference between
\code{Exp} and \code{Acc} is that the first is used for sequential
computations and the second is used for collective operations like map
or fold. Let us look at the type of the fold operation:

\begin{lstlisting}
  fold :: (Shape ix, Elt a) =>
          (Exp a -> Exp a -> Exp a) ->
          Exp a ->
          Acc (Array (ix :. Int) a) ->
          Acc (Array ix a)
\end{lstlisting}

It is a little complicated because of the constraint on the type of
elements allowed (\code{Elt a}) and because Accelerate works with
multi-dimensional arrays (the \code{Shape ix} constraint). The
important part is that the first argument is a function with the type
\code{Exp a -> Exp a -> Exp a}. This means that only a sequential
function can be executed in parallel. This is called flat data
parallelism - parallel code can not create more parallelism. This
constraint is due to the difficulty of efficiently compiling nested
data parallel programs to GPU code (this is an open research problem).

Because Accelerate does not yet support \code{Exp} computations that
return arrays (i.e. sequential computations on arrays), this meant
that it was not possible to write a sequential sorting function and
use it in parallel. Instead, the sorting function is written using
\code{Acc} computations and applied to the vectors of each query
sequentially.

This raised another issue. Accelerate's combinator functions enable
the programmer to write parallel loops. But there are very limited
features for writing sequential loops where the next iteration depends
on the previous one. The modified insertion sort code is:

\begin{lstlisting}
insertionSortK k n input =
  foldl1 (>->)
  [ maybeInsert i | i <- [k .. n-1] ]
  initial
  where
    initial = insertionSort $
              A.take (constant k) input

    maybeInsert i vec =
      let elem = input A.!! constant i
          last = vec A.!! (size vec - 1)
      in
       acond (A.snd last >* A.snd elem)
       (insert elem $ A.init vec)
       vec
\end{lstlisting}

In the code, $n$ is the length of the input and \code{initial} is the
first $k$ elements of the input sorted using regular insertion
sort. Then, for each index $i$ from $k$ to $n-1$, a function is
created with \code{maybeInsert}. This function, when given a $k$
element sorted vector, inserts the $i$-th element of the original
input into this vector if it is smaller than the greatest element in
the vector. A list of these functions is then composed into a single
function by reducing with the \code{>->} operator which is then
applied to the \code{initial} vector. This essentially creates an
unrolled loop which would be fine if the number of iterations was
small. But when we sort a vector with thousands of points it will take
a very long time just to compile the CUDA program. I did not find a
way to write an \code{Acc} loop like this in another way.

\subsection{Implementation using sorting networks based on Batcher's odd-even merge sort}

If we can not sort separate vectors in parallel, another way of
optimising would be a parallel sort of each vector. Ken Batcher's
odd-even mergesort was implemented for this reason \cite{oddeven}. The
odd-even mergesort algorithm recursively splits the input like regular
mergesort but the merging algorithm is different \cite{oddeven}:

\begin{algorithm}
  \KwIn{sequence $a_0, \ldots, a_{n-1}$ of length $n > 1$ whose two
  halves $a_0, \ldots, a_{n/2-1}$ and $a_{n/2}, \ldots, a_{n-1}$ are
  sorted ($n$ is a power of two).}

  \eIf{$n > 2$}{
    apply odd-even merge recursively to the even subsequence $a_0,
    a_2, \ldots, a_{n-2}$ and to the odd subsequence $a_1, a_3,
    \ldots, a_{n-1}$; \

    compare $a_i$ and $a_{i+1}$ for all $i \in \{1, 3, 5, \ldots,
    n-3\}$
  }{
    compare $a_0$ and $a_1$
  }
  \caption{Odd-even merge}
\end{algorithm}

With even splitting as in merge sort and using the presented merge
function, the odd-even mergesort algorithm is data-independent. The
comparisons made are dependent on the size of the data but not the
values. Comparisons of different pairs can be executed in
parallel. The algorithm essentially creates sets of independent
comparisons (called stages).

Odd-even merge sort can be used to create a sorting network which is a
graph of comparison gates. Sorting networks are sometimes implemented
in hardware and often on GPUs. Figure ~\ref{fig:sortingnetwork} is an
example of a three stage sorting network used to sort four values. The
arrow indicates where the larger element of the pair should go.

\begin{figure}[h]
  \begin{center}
    \includegraphics{sortingnetwork.eps}
  \end{center}
  \caption{A sorting network for 4-element vectors.}
  \label{fig:sortingnetwork}
\end{figure}

The sorting network can be generated on the CPU recursively. First a
sorting network for two elements is created. Two of these are combined
to create a four element sorting network by adding comparisons of the
merge procedure. The resulting network can be combined with itself to
create a sorting network for eight elements and so on. The complexity
of Batcher's odd-even mergesort is $\Oh(n \log(n)^2)$. If the
parameter $k$ is large, the insertion sort function described earlier
would become close to a regular insertion sort which has complexity
$\Oh(n^2)$. So for large $k$, a parallel sorting algorithm like this
could even be more efficient than the described algorithm of Garcia
et.al.

The Swiss-system tournament network \cite{swisstournament} can be used
to create a sorting network that would not require to sort the whole
vector. This network is used in tournaments where participants compete
in pairs. This kind of a network can be used to efficiently find $k$
points close to the query. The disadvantage is that it is an
approximate algorithm since the goal of a tournament is to determine
the winner and the rest of the positions are not as important.


\section{Performance}

The computer used has an Intel i5-4570 processor and an Nvidia GTX 760
graphics processing unit.

The insertion sort based code is very inefficient. As described in the
implementation section, it creates a very large program that takes a
long time to compile. If the dimensionality of the search space, $k$
and number of queries were all set to 3 and the number of data points
was 256, it took 297s to run on the GPU because of the compilation
time while subsequent iterations with cached code took 3.33s on average
(which is still poor but includes the latency caused by
sending/receiving data from the GPU). Running the same code with the
insertion sort based algorithm using the CPU backend took 2.0s on
average.

Using the same parameters, the odd-even merge sort algorithm took
1.69s on the CPU. While testing on the GPU, another problem
occured. The CUDA backend has an internal bug and often crashed with
an error. When it did succeed the runtime was about 2.73s (including
the communication latency). The problem with sorting networks is that
they also take memory and need to be transfered to the GPU.

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=0.5\textwidth]{plot.eps}
  \end{center}
  \caption{Comparison of running times of the two algorithms}
  \label{fig:runtimes}
\end{figure}

Figure \ref{fig:runtimes} shows the running times of the two
algorithms and the CUDA implementation of \cite{garcia2008fast} in the
range of input sizes that didn't crash the CUDA backend of Accelerate
too often.

When profiling with Nvidia's Visual Profiler, it seemed that the
compiled CUDA code is very poor. When running the odd-even merge sort
based algorithm, the data was copied to the GPU in very small chunks
which causes a lot of overhead. Then a large number of GPU streams
were created, each of which only ran for less than a nanoseconds. The
different streams did not exhibit any parallelism. The profiler
claimed that most cores were idling during the run of the program.

If the Accelerate program is poorly written this would confirm that
writing efficient programs in Accelerate would still require a lot of
knowledge about how Accelerate and GPUs work. To test this, the dot
product example was also run. This can be efficiently parallelised and
Trevor McDonell's benchmarking \cite{mcdonell} showed it to be close
to the dot product function in the CUDA library. On the testing
computer and with 10 million element vectors, it ran almost 5 times
slower than the straightforward equivalent Haskell code. There is
probably something wrong with some version of a piece of software in
the pipeline (Accelerate, Accelerate CUDA backend, CUDA compiler,
graphics card driver) and the results are not valid.


\section{Conclusion}

Accelerate turned out to be very difficult to use to program an
efficient nearest neighbour search. The main issues were stability and
limited iteration constructs.

It is probable that I did not understand Accelerate well enough to
write efficient programs. Trevor McDonell wrote multiple efficient
programs for his thesis \cite{mcdonell}, for example Black-Scholes
option pricing (a partial differential equation), N-body gravitational
simulation, sparse matrix vector multiplication, fluid flow, shortest
paths in a graph and k-means clustering. Many of the programs were
close to the performance of optimised CUDA programs. On some data
sets, the matrix vector multiplication code was significantly faster
than CUSP which is an optimised CUDA library for sparse matrix vector
multiplication.

The issue of sequential though seems to be common. For example,
McDonell's N-body simulation computed interactions between pairs of
bodies and reduced the result in parallel. This was 10 times slower
than the CUDA program. An improved version used \code{sfoldl} which is
a sequential \code{Exp} computation for reducing an
array. \code{sfoldl} can be applied using a parallel combinator. This
improved the performance significantly. Sequential array operations
that can be applied in parallel are exactly the solution that was
required in the insertion sort based algorithm. Unfortunately there
are currently very few of them. The authors discussed adding more in
Accelerate's issue tracker.

Another possible solution would be to remove the difference between
\code{Acc} and \code{Exp} computations. This would also make the
language easier to understand. It would require supporting nested data
parallelism. Efficiently compiling programs with nested data
parallelism to GPUs is an open research problem. The authors of
Accelerate have said that they are interested in this and Rob
Clifton-Everest is working on it as part of his PhD.


\bibliographystyle{ieeetr}
\bibliography{bibliography}

\end{document}
